.. Java Guide documentation master file, created by
   sphinx-quickstart on Thu May  3 17:11:52 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Java Guide
==========

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: Contents:

   java/basic


