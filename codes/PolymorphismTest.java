// PolymorphismTest.java

public class PolymorphismTest{
    public static void main(String[] args){
        Animal a = new Animal();
        Bird b = new Bird();

        a.scarySound();
        b.scarySound();
    }
}
