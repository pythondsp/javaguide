// incrementEx.java

public class incrementEx{
    public static void main(String[] args){
        int i = 1;
        System.out.printf("i = %d\n\n", i);
        
        // print first, then increment
        System.out.printf("i++ = %d (i.e. access first and then increment): \n", i++); // 1
        System.out.printf("i = %d\n\n", i); // 2 (i.e. i is incremented by above statement)

        // increment first, then print
        System.out.printf("++i = %d (i.e. increment first and then access):\n", ++i); // 3
        System.out.printf("i = %d\n", i); // 3
    }
}