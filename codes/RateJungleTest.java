// RateJungleTest.java

public class RateJungleTest{
    public static void main(String[] args){
        RateJungle m = new RateJungle("Meher");
        RateJungle k = new RateJungle("Krishna", 5);
        RateJungle p = new RateJungle();

        p.setVisitorName("Patel");
        p.setFeedback(2);

        m.printRating();
        k.printRating();
        p.printRating();
    }
}
