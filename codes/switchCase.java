// switchCase.java

import java.util.Scanner; // used to read input

public class switchCase{
    public static void main(String[] args){
    	// Create object of Scanner
        Scanner scn = new Scanner(System.in);
    	
        System.out.print("Enter Grade: ");
        String input = scn.next();    	
    	char grade = input.charAt(0);
        
    	switch(grade){
	        case ('A'): 
	        	System.out.printf("A: performance is excellent\n");
	            break;
	        case 'B' :
	        	System.out.printf("B: performance is good\n");
	            break;
	        case 'C' :
	        	System.out.printf("C: performance is not bad\n");
	            break;
	        default :
	        	System.out.printf("Invalid grade\n"); 	
    	}
    }
}