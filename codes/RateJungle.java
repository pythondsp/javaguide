// RateJungle.java

public class RateJungle extends Jungle{
    private int feedback;
    private String name;

    // constructor 1
    public RateJungle(String name){
        super(name); // call the base class constructor
        feedback = 0; // set feedback to 0
    }

    // constructor 2
    public RateJungle(String name, int val){
        super(name);
        feedback = val;
    }

    // constructor 3 : empty constructor
    public RateJungle(){
    }

    public void setFeedback(int val){
        feedback = val;
    }

    public void printRating(){
        System.out.printf("Thanks %s\n", getVisitorName());
        System.out.printf("Your feedback is set as : %d\n", feedback);
    }
}
            
     

