// StaticEx.java

public class StaticEx{
    
    int a = 2;  // non-static global variables
    static int b = 4; // static global variable

    // non-static function
    public void sum2Num(int x, int y){
        System.out.println("Sum = " + (x+y));
    }

    // static function 
    public static void diff2Num(int x, int y){
        System.out.println("Difference = " + (x-y));

    }

    public static void main(String[] args){

        diff2Num(2, 3);

        // below is not allowed as sum2Num is not static
        // sum2Num(2, 3);  
        // First create an instance of class and use non-static functions
        StaticEx s1 = new StaticEx();
        s1.sum2Num(2, 3);
        // same is applicable for non-static variables
        s1.sum2Num(s1.a, b);

    }
}