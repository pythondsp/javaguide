// ReadEx.java

import java.util.Scanner; // used to read input

public class ReadEx{
    public static void main(String[] args){
        // Create object of Scanner
        Scanner scn = new Scanner(System.in);
        
        int x, y, z; // variables

        // print
        System.out.print("Enter first number: ");
        x = scn.nextInt(); // read x

        System.out.print("Enter second number: ");
        y = scn.nextInt(); // read y

        z = x+y; // add

        // printf
        System.out.printf("Sum = %d\n", z);
    }
}






