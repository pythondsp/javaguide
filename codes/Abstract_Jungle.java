// Abstract_Jungle.java

// abstract class
public abstract class Abstract_Jungle{

    private String visitorName;

    // set (save) visitor name
    public void setVisitorName(String name){
        visitorName = name;
    }

    // get visitor name
    public String getVisitorName(){
        return visitorName;
    }

    // print message 
    public void welcomeMessage(){
        System.out.printf("Hello %s! Welcome to Jungle\n", getVisitorName());
    }

    // abstract method
    public abstract void scarySound();
}
