// whileLoop.java

public class whileLoop{
    public static void main(String[] args){
    	int i=0;

    	// print 0 to 4
        while(i < 5){
            System.out.printf("%d, ", i); //0, 1, 2, 3, 4,
            i = i+1;
        }

        // value of i is 5
        System.out.printf("\n");
        System.out.printf("Value of i = %d\n", i); // Value of i = 5
    }
}