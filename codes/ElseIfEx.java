// ElseIfEx.java

import java.util.Scanner; // used to read input

public class ElseIfEx{
    public static void main(String[] args){
    	// Create object of Scanner
        Scanner scn = new Scanner(System.in);
    	
        System.out.print("Enter Grade: ");
        String input = scn.next();    	
    	char grade = input.charAt(0);
        
        // if grade is A or a
        if (grade == 'A' || grade == 'a')   // logical operator
        	System.out.printf("Excellent Student\n");
        else if ((grade == 'B') || (grade == 'b'))   
        	System.out.printf("Very Good Student\n");
        else if (grade == 'C' || grade == 'c')
        	System.out.printf("Good Student\n");
        else if (grade == 'D' || grade == 'd')
        	System.out.printf("Need improvements\n");
        else
        	System.out.printf("Invalid grade\n");
    }
}